# Git Fix Um
## A Choose Your Own Adventure Style Guide to Fixing Git Merge Issues

Not affiliated with Chooseco, LLC's "Choose Your Own Adventure"ⓡ. Good books, but a little light on the details of recovering from git merge errors to my taste.

### Disclaimer

Information is not promised or guaranteed to be correct, current, or complete, and may be out of date and may contain technical inaccuracies or typographical errors. Any reliance on this material is at your own risk. No one assumes any responsibility (and everyone expressly disclaims responsibility) for updates to keep information current or to ensure the accuracy or completeness of any posted information. Accordingly, you should confirm the accuracy and completeness of all posted information before making any decision related to any and all matters described.


### Map and trailguide

- start
  - changeFix
    - badMerge
    - badRebase
    - committed
      - reallyCommitted
        - pushed
          - restoreFile
          - newMerge
          - simpleNew
          - fixIt
          - branchNewMerge
          - old
        - unpushed
          - discardUnpushed
          - replaceUnpushed
          - fixUnpushed
            - changeLast
              - removeLast
              - updateLast
              - reworkLast
            - removeLast
            - undoTip
            - changeDeep
              - removeDeep
              - modifyDeep
                - bulkRewrite
                  - filterBranch
                  - bfg
                - singleDeepChange
                  - singleDeepMerge
                  - singleDeepSimple
            - restoreFile
            - moveCommit
      - everythingUncommitted
      - uncommittedCommit
    - uncommitted
      - everythingUncommitted
      - somethingsUncommitted
      - uncommittedCommit
  - lostAndFound