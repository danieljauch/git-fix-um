/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"
/*** [end of imports] ***/

export default class Tile extends Component {
  render() {
    const { goTo, tile } = this.props
    const { title, signPost, solution, trails } = tile

    return (
      <section className="story__tile">
        <header className="story__header">
          <h4 className="title story__title">{title}</h4>
          <p className="subtitle story__subtitle">{signPost}</p>
          {!!solution && solution}

          <hr className="hr" />

          <ul className="story__trail--list list__wrap">
            {trails.map(trail => (
              <Trail {...trail} goTo={goTo} key={`trailTo_${trail.direction}`} />
            ))}
          </ul>
        </header>
      </section>
    )
  }
}

const Trail = props => {
  const { sign, goTo, direction } = props

  return (
    <li className="trail story__trail--item list__item btn" onClick={() => goTo(sign, direction)}>
      {sign}
    </li>
  )
}
