/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"
/*** [end of imports] ***/

export default class Collapse extends Component {
  state = {
    open: false
  }

  componentWillMount = () => {
    this.setState({
      open: this.props.defaultState
    })
  }

  toggle = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const { children, buttonText, classes } = this.props

    let classList = ["collapse__wrap"]

    if (this.state.open) {
      classList.push("open")
    }
    if (classes) {
      classList.push(classes)
    }

    return (
      <div className={classList.join(" ")}>
        <button className="btn collapse__toggle-btn" onClick={() => this.toggle()}>
          {buttonText || "Collapse"}
        </button>
        <div className="collapse__content">{children}</div>
      </div>
    )
  }
}
