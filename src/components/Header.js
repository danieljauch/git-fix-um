/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"

// Components
import Collapse from "./Collapse"
/*** [end of imports] ***/

export default class Header extends Component {
  render() {
    const { goTo } = this.props

    return (
      <header className="header__wrap app__header">
        <h1 className="app__title title">A Git Fix Wizard</h1>
        
        <Collapse classes="app__subtitle subtitle" buttonText="Toggle instructions">
          <p>
            This app is an attempt to be a fairly comprehensive guide to recovering from what you
            did not mean to do when using git. It isn't that git is so complicated that you need a
            large document to take care or your particular problem, it is more that the set of
            things that you might have done is so large that different techniques are needed
            depending on exactly what you have done and what you want to have happen.
          </p>
          <p>
            Strongly consider taking a backup of your current working directory and .git to avoid
            any possibility of losing data as a result of the use or misuse of these instructions.
            We promise to laugh at you if you fail to take a backup and regret it later.
          </p>
          <p>
            Answer the questions posed by clicking the link for that section. A section with no
            links is a terminal node and you should have solved your problem by completing the
            suggestions posed by that node (if not, then report the chain of answers you made on
            #git or some other git resource and explain further why the proposed answer doesn't
            help). This is not a document to read linearly.
          </p>
        </Collapse>

        <button className="btn restart-btn" onClick={() => goTo("", "start")}>
          Start over
        </button>
      </header>
    )
  }
}
