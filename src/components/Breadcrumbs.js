/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"
/*** [end of imports] ***/

export default class Breadcrumbs extends Component {
  render() {
    const { crumbs } = this.props

    return (
      <aside className="app__aside breadcrumbs__wrap">
        {crumbs.length > 0 ? (
          <ul className="breadcrumbs__list list__wrap">
            {crumbs.map((crumb, _index) => (
              <Crumb label={crumb} key={`crumb_${_index}`} />
            ))}
          </ul>
        ) : (
          <p className="breadcrumbs__none-message">Make a choice and start your adventure!</p>
        )}
      </aside>
    )
  }
}

const Crumb = ({ label }) => <li className="breadcrumbs__crumb list__item">{label}</li>
