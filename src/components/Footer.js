/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"
/*** [end of imports] ***/

// credit: http://sethrobertson.github.io/GitFixUm/fixup.html

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer__wrap app__footer">
        <div className="credit">
          Made with love by <a href="http://danieljauch.com/">Daniel Jauch</a> based on the{" "}
          <a href="http://sethrobertson.github.io/GitFixUm/fixup.html">great original</a> by{" "}
          <a href="http://sethrobertson.github.io/">Seth Robertson</a>.{" "}
          <a href="https://bitbucket.org/danieljauch/git-fix-um/src/master/">Check out the code</a>{" "}
          and fork it for yourself.
        </div>
      </footer>
    )
  }
}
