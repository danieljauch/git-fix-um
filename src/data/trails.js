import React, { Fragment } from "react"

const trails = {
  // Step 1
  start: {
    title: "Are you trying to find that which is lost or fix a change that was made?",
    signPost:
      "Due to previous activities (thrashing about), you may have lost some work which you would like to find and restore. Alternately, you may have made some changes which you would like to fix. Fixing includes updating, rewording, and deleting or discarding.",
    solution: false,
    trails: [
      {
        sign: "Fix a change",
        direction: "changeFix"
      },
      {
        sign: "Find what is lost",
        direction: "lostAndFound"
      }
    ]
  },

  // Step 2
  changeFix: {
    title: "Have you committed?",
    signPost:
      "If you have not yet committed that which you do not want, git does not know anything about what you have done yet, so it is pretty easy to undo what you have done.",
    solution: false,
    trails: [
      {
        sign: "I am in the middle of a bad merge",
        direction: "badMerge"
      },
      {
        sign: "I am in the middle of a bad rebase",
        direction: "badRebase"
      },
      {
        sign: "Yes, commits were made",
        direction: "committed"
      },
      {
        sign: "No, I have not yet committed",
        direction: "uncommitted"
      }
    ]
  },
  lostAndFound: {
    title: "I have lost some commits I know I made",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          First make sure that it was not on a different branch. Try{" "}
          <code className="inline">git log -Sfoo --all</code> where "foo" is replaced with something
          unique in the commits you made. You can also search with{" "}
          <code className="inline">gitk --all --date-order</code> to see if anything looks likely.
        </p>

        <p>
          Check your stashes, <code className="inline">git stash list</code>, to see if you might
          have stashed instead of committing. You can also visualize what the stashes might be
          associated with via:
        </p>

        <code className="block">
          gitk --all --date-order $(git stash list | awk -F: '{"{"}
          print $1
          {"}"}
          ;')
        </code>

        <p>
          Next, you should probably look in other repositories you have lying around including ones
          on other hosts and in testing environments, and in your backups.
        </p>

        <p>
          Once you are fully convinced that it is well and truly lost, you can start looking
          elsewhere in git. Specifically, you should first look at the reflog which contains the
          history of what happened to the tip of your branches for the past two weeks or so. You can
          of course say <code className="inline">git log -g</code> or{" "}
          <code className="inline">git reflog</code> to view it, but it may be best visualized with:
        </p>

        <code className="block">gitk --all --date-order $(git reflog --pretty=%H)</code>

        <p>
          Next you can look in git's lost and found. Dangling commits get generated for many good
          reasons including resets and rebases. Still those activities might have mislaid the
          commits you were interested in. These might be best visualized with{" "}
          <code className="inline">
            gitk --all --date-order $(git fsck | grep "dangling commit" | awk '{"{"}
            print $3;
            {"}"}
            ')
          </code>
        </p>

        <p>
          The last place you can look is in dangling blobs. These are files which have been{" "}
          <code className="inline">git add</code>
          ed but not attached to a commit for some (usually innocuous) reason. To look at the files,
          one at a time, run:
        </p>

        <code className="block">
          git fsck | grep "dangling blob" | while read x x s; do git show $s | less; done
        </code>

        <p>
          Once you find the changes you are interested in, there are several ways you can proceed.
          You can <code className="inline">git reset --hard SHA</code> your current branch to the
          history and current state of that SHA (probably not recommended for stashes), you can{" "}
          <code className="inline">git branch newbranch SHA</code> to link the old history to a new
          branch name (also not recommended for stashes), you can{" "}
          <code className="inline">git stash apply SHA</code> (for the non-index commit in a
          git-stash), you can <code className="inline">git stash merge SHA</code> or{" "}
          <code className="inline">git cherry-pick SHA</code> (for either part of a stash or
          non-stashes), etc.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },

  // Step 3
  badMerge: {
    title: "Recovering from a borked/stupid/moribund merge",
    signPost:
      "So, you were in the middle of a merge, have encountered one or more conflicts, and you have now decided that it was a big mistake and want to get out of the merge.",
    solution: (
      <Fragment>
        <p>
          The fastest way out of the merge is <code className="inline">git merge --abort</code>
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  badRebase: {
    title: "Recovering from a borked/stupid/moribund rebase",
    signPost:
      "So, you were in the middle of a rebase, have encountered one or more conflicts, and you have now decided that it was a big mistake and want to get out of the merge.",
    solution: (
      <Fragment>
        <p>
          The fastest way out of the merge is <code className="inline">git rebase --abort</code>
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  committed: {
    title: "Do you have uncommitted stuff in your working directory?",
    signPost: (
      <Fragment>
        <p>
          So you have committed. However, before we go about fixing or removing whatever is wrong,
          you should first ensure that any uncommitted changes are safe, by either committing them (
          <code className="inline">git commit</code>) or by stashing them (
          <code className="inline">git stash save "message"</code>) or getting rid of them.
        </p>

        <p>
          <code className="inline">git status</code> will help you understand whether your working
          directory is clean or not. It should report nothing for perfect safety ("Untracked files"
          only are sometimes safe.)
        </p>
      </Fragment>
    ),
    solution: false,
    trails: [
      {
        sign: "No, I have no changes/working directory is clean",
        direction: "reallyCommitted"
      },
      {
        sign: "Yes, I have bad changes/working directory is dirty: discard it",
        direction: "everythingUncommitted"
      },
      {
        sign: "Yes, I have good changes/working directory is dirty: save it",
        direction: "uncommittedCommit"
      }
    ]
  },
  uncommitted: {
    title: "Discard everything or just some things?",
    signPost:
      "So you have not yet committed, the question is now whether you want to undo everything which you have done since the last commit or just some things, or just save what you have done?",
    solution: false,
    trails: [
      {
        sign: "Discard everything",
        direction: "everythingUncommitted"
      },
      {
        sign: "Discard some things",
        direction: "somethingsUncommitted"
      },
      {
        sign: "I want to save my changes",
        direction: "uncommittedCommit"
      }
    ]
  },

  // Step 4
  reallyCommitted: {
    title: "Have you pushed?",
    signPost: (
      <Fragment>
        <p>
          So you have committed, the question is now whether you have made your changes (or at least
          the changes you are interesting in "fixing") publicly available or not. Publishing history
          is a seminal event.
        </p>

        <p>
          If you are dealing with commits someone else made, then this question covers whether they
          have pushed, and since you have their commits, the answer is almost certainly "yes".
        </p>

        <p>
          Please note in any and all events, the recipes provided here will typically (only one
          exception which will self-notify) only modify the current branch you are on. Specifically
          any tags or branches involving the commit you are changing or a child of that commit will
          not be modified. You must deal with those separately. Look at{" "}
          <code className="inline">gitk --all --date-order</code> to help visualize everything what
          other git references might need to be updated.
        </p>

        <p>
          Also note that these commands will fix up the referenced commits in your repository. There
          will be reflog'd and dangling commits holding the state you just corrected. This is
          normally a good thing and it will eventually go away by itself, but if for some reason you
          want to cut your seat belts, you can expire the reflog now and garbage collect with
          immediate pruning.
        </p>
      </Fragment>
    ),
    solution: false,
    trails: [
      {
        sign: "Yes, pushes were made",
        direction: "pushed"
      },
      {
        sign: "No pushes",
        direction: "unpushed"
      }
    ]
  },
  everythingUncommitted: {
    title: "How to undo all uncommitted changes",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          So you have not yet committed and you want to undo everything. Well,{" "}
          <a href="http://sethrobertson.github.com/GitBestPractices/">best practice</a> is for you
          to stash the changes in case you were mistaken and later decide that you really wanted
          them after all. <code className="inline">git stash save "description of changes"</code>.
          You can revisit those stashes later <code className="inline">git stash list</code> and
          decide whether to <code className="inline">git stash drop</code> them after some time has
          past. Please note that untracked and ignored files are not stashed by default. See
          "--include-untracked" and "--all" for stash options to handle those two cases.
        </p>

        <p>
          However, perhaps you are confident (or arrogant) enough to know for sure that you will
          never ever want the uncommitted changes. If so, you can run{" "}
          <code className="inline">git reset --hard</code>, however please be quite aware that this
          is almost certainly a completely unrecoverable operation. Any changes which are removed
          here cannot be restored later. This will not delete untracked or ignored files. Those can
          be deleted with <code className="inline">git clean -nd</code>{" "}
          <code className="inline">git clean -ndX</code> respectively, or{" "}
          <code className="inline">git clean -ndx</code> for both at once. Well, actually those
          command do not delete the files. They show what files will be deleted. Replace the "n" in
          "-nd…" with "f" to actually delete the files.{" "}
          <a href="http://sethrobertson.github.com/GitBestPractices/">Best practice</a> is to ensure
          you are not deleting what you should not by looking at the moribund filenames first.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  uncommittedCommit: {
    title: "How to save uncommitted changes",
    signPost: "There are five ways you can save your uncommitted change.",
    solution: (
      <Fragment>
        <table>
          <tr>
            <th>Description</th>
            <th>Command</th>
          </tr>
          <tr>
            <td>Commit them on the local branch.</td>
            <td>
              <code className="inline">git commit -am "My descriptive message"</code>
            </td>
          </tr>
          <tr>
            <td>Commit them on another branch, no checkout conflicts.</td>
            <td>
              <code className="inline">
                git checkout otherbranch &amp;&amp; git commit -am "My descriptive message"
              </code>
            </td>
          </tr>
          <tr>
            <td>Commit them on another branch, conflicts.</td>
            <td>
              <code className="inline">
                git stash; git checkout otherbranch; git stash apply; : "resolve conflicts"; git
                commit -am "My descriptive message"; git stash drop
              </code>
            </td>
          </tr>
          <tr>
            <td>Commit them on a new branch.</td>
            <td>
              <code className="inline">
                git checkout -b newbranch; git commit -am "My descriptive message"
              </code>
            </td>
          </tr>
          <tr>
            <td>Stash them for a rainy day.</td>
            <td>
              <code className="inline">git stash save "my descriptive name"</code>
            </td>
          </tr>
        </table>

        <p>
          Using <code className="inline">git add -p</code> to add/commit only some changes to make
          multiple commits is left as an exercise for the reader.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  somethingsUncommitted: {
    title: "How to undo some uncommitted changes",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          So you have not yet committed and you want to undo some things, well{" "}
          <code className="inline">git status</code> will tell you exactly what you need to do. For
          example:
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              # On branch master # Changes to be committed: # (use "git reset HEAD &lt;file&gt;..."
              to unstage) # # new file: .gitignore # # Changes not staged for commit: # (use "git
              add &lt;file&gt;..." to update what will be committed) # (use "git checkout --
              &lt;file&gt;..." to discard changes in working directory) # # modified: A # #
              Untracked files: # (use "git add &lt;file&gt;..." to include in what will be
              committed) # # C
            </code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          However, the <code className="inline">git checkout</code> in file mode is a command that
          cannot be recovered from—the changes which are discarded most probably cannot be
          recovered. Perhaps you should run{" "}
          <code className="inline">git stash save -p "description"</code>
          instead, and select the changes you no longer want to be stashed instead of zapping them.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },

  // Step 5
  pushed: {
    title: "Can you make a positive commit to fix the problem and what is the fix class?",
    signPost: (
      <p>
        Rewriting public history is a bad idea. It requires everyone else to do special things and
        you must publicly announce your failure. Ideally you will create either a commit to just fix
        the problem, or a new <code className="inline">git revert</code> commit to create a new
        commit which undoes what the commit target of the revert did.
      </p>
    ),
    solution: false,
    trails: [
      {
        sign:
          "Yes, I can make a new commit but the bad commit trashed a particular file in error (among other good things I want to keep)",
        direction: "restoreFile"
      },
      {
        sign:
          "Yes, I can make a new commit and the bad commit is a merge commit I want to totally remove",
        direction: "newMerge"
      },
      {
        sign:
          "Yes, I can make a new commit but the bad commit is a simple commit I want to totally remove",
        direction: "simpleNew"
      },
      {
        sign: "Yes, I can make a new commit and the bad commit has an error in it I want to fix",
        direction: "fixIt"
      },
      {
        sign:
          "Yes, I can make a new commit but history is all messed up and I have a replacement branch",
        direction: "branchNewMerge"
      },
      {
        sign: "No, I am a bad person and must rewrite published history",
        direction: "old"
      }
    ]
  },
  unpushed: {
    title: "Do you want to discard all unpushed changes on this branch?",
    signPost: (
      <p>
        There is a shortcut in case you want to discard all changes made on this branch since you
        have last pushed or in any event, to make your local branch identical to "upstream".
        Upstream, for local tracking branches, is the place you get history from when you{" "}
        <code className="inline">git pull</code>: typically for master it might be origin/master.
        There is a variant of this option which lets you make your local branch identical to some
        other branch or ref.
      </p>
    ),
    solution: false,
    trails: [
      {
        sign: "Yes, I want to discard all unpushed changes",
        direction: "discardUnpushed"
      },
      {
        sign: "Yes, and I want to make my branch identical to some non-upstream ref",
        direction: "replaceUnpushed"
      },
      {
        sign: "No, I want to fix some unpushed changes",
        direction: "fixUnpushed"
      }
    ]
  },

  // Step 6
  restoreFile: {
    title: "Making a new commit to restore a file deleted earlier",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          The file may have been deleted or every change to that file in that commit (and all
          commits since then) should be destroyed. If so, you can simply checkout a version of the
          file which you know is good.
        </p>

        <p>
          You must first identify the SHA of the commit containing the good version of the file. You
          can do this using <code className="inline">gitk --date-order</code> or using{" "}
          <code className="inline">git log --graph --decorate --oneline</code> or perhaps{" "}
          <code className="inline">git log --oneline -- filename</code> You are looking for the 40
          character SHA-1 hash ID (or the 7 character abbreviation). Yes, if you know the "&#94;" or
          "~" shortcuts you may use those.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git checkout SHA -- path/to/filename</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Obviously replace "SHA" with the reference that is good. You can then add and commit as
          normal to fix the problem.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  newMerge: {
    title: "Reverting a merge commit",
    signPost: "Oh dear. This is going to get complicated.",
    solution: (
      <Fragment>
        <p>
          To create an positive commit to remove the effects of a merge commit, you must first
          identify the SHA of the commit you want to revert. You can do this using{" "}
          <code className="inline">gitk --date-order</code> or using{" "}
          <code className="inline">git log --graph --decorate --oneline</code> You are looking for
          the 40 character SHA-1 hash ID (or the 7 character abbreviation). Yes, if you know the
          "&#94;" or "~" shortcuts you may use those.
        </p>

        <p>
          Undoing the file modifications caused by the merge is about as simple as you might hope.{" "}
          <code className="inline">git revert -m 1 SHA</code>. (Obviously replace "SHA" with the
          reference you want to revert;
          <code className="inline">-m 1</code> will revert changes from all but the first parent,
          which is almost always what you want.) Unfortunately, this is just the tip of the iceberg.
          The problem is, what happens months later, long after you have exiled this problem from
          your memory, when you try again to merge these branches (or any other branches they have
          been merged into)? Because git has it tracked in history that a merge occurred, it is not
          going to attempt to remerge what it has already merged, and even worse, if you merge
          <em>from</em> the branch where you did the revert you will undo the changes on the branch
          where they were made. (Imagine you revert a premature merge of a long-lived topic branch
          into master and later merge master into the topic branch to get other changes for
          testing.)
        </p>

        <p>
          One option is actually to do this reverse merge immediately, annihilating any changes
          before the bad merge, and then to "revert the revert" to restore them. This leaves the
          changes removed from the branch you mistakenly merged to, but present on their original
          branch, and allows merges in either direction without loss. This is the simplest option,
          and in many cases, can be the best.
        </p>

        <p>
          A disadvantage of this approach is that <code className="inline">git blame</code> output
          is not as useful (all the changes will be attributed to the revert of the revert) and{" "}
          <code className="inline">git bisect</code> is similarly impaired. Another disadvantage is
          that you must merge all current changes in the target of the bad merge back into the
          source; if your development style is to keep branches clean, this may be undesirable, and
          if you rebase your branches (e.g. with
          <code className="inline">git pull --rebase</code>
          ), it could cause complications unless you are careful to use{" "}
          <code className="inline">git rebase -p</code> to preserve merges.
        </p>

        <p>
          In the following example please replace $destination with the name of the branch that was
          the destination of the bad merge, $source with the name of the branch that was the source
          of the bad merge, and $sha with the SHA-1 hash ID of the bad merge itself.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              git checkout $destination git revert $sha # save the SHA-1 of the revert commit to
              un-revert it later revert=`git rev-parse HEAD` git checkout $source git merge
              $destination git revert $revert
            </code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Another option is to abandon the branch you merged from, recreate it from the previous
          merge-base with the commits since then rebased or cherry-picked over, and use the
          recreated branch from now on. Then the new branch is unrelated and will merge properly. Of
          course, if you have pushed the donor branch you cannot use the same name (that would be
          rewriting public history and is bad) so everyone needs to remember to use the new branch.
          Hopefully you have something like
          <a href="https://github.com/sitaramc/gitolite">gitolite</a> where you can close the old
          branch name.
        </p>

        <p>
          This approach has the advantage that the recreated donor branch will have cleaner history,
          but especially if there have been many commits (and especially merges) to the branch, it
          can be a lot of work. At this time, I will not walk you through the process of recreating
          the donor branch. Given sufficient demand I can try to add that. However, if you look at{" "}
          <a href="https://www.kernel.org/pub/software/scm/git/docs/howto/revert-a-faulty-merge.txt">
            howto/revert-a-faulty-merge.txt
          </a>
          (also shipped as part of the git distribution) it will provide more words than you can
          shake a stick at.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  simpleNew: {
    title: "Reverting an old simple pushed commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          To create an positive commit to remove the effects of a simple (non-merge) commit, you
          must first identify the SHA of the commit you want to revert. You can do this using{" "}
          <code className="inline">gitk --date-order</code> or using{" "}
          <code className="inline">git log --graph --decorate --oneline</code> You are looking for
          the 40 character SHA-1 hash ID (or the 7 character abbreviation). Yes, if you know the
          "&#94;" or "~" shortcuts you may use those.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git revert SHA</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Obviously replace "SHA" with the reference you want to revert. If you want to revert
          multiple SHA, you may specify a range or a list of SHA.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  fixIt: {
    title: "Making a new commit to fix an old commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          If the problem in the old commit is just something was done incorrectly, go ahead and make
          a normal commit to fix the problem. Feel free to reference the old commit SHA in the
          commit message, and if you are into the blame-based development methodology, make fun of
          the person who made the mistake (or someone who recently left if you made the mistake).
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  branchNewMerge: {
    title: "Rewriting an old branch with a new branch with a new commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          If the state of a branch is contaminated beyond repair and you have pushed that branch or
          otherwise do not want to rewrite the existing history, then you can make a new commit
          which overwrites the original branch with the new one and pretends this was due to a
          merge. The command is a bit complicated, and will get rid of all ignored or untracked
          files in your working directory, so please be sure you have properly backed up everything.
        </p>

        <p>
          In the following example please replace $destination with the name of the branch whose
          contents you want to overwrite. $source should be replaced with the name of the branch
          whose contents are good.
        </p>

        <p>
          You actually are being provided with two methods. The first set is more portable but
          generates two commits. The second knows about the current internal files git uses to do
          the necessary work in one commit. Only one command is different and a second command runs
          at a different time.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              # Portable method to overwrite one branch with another in two commits git clean -dfx
              git checkout $destination git reset --hard $source git reset --soft ORIG_HEAD git add
              -fA . git commit -m "Rewrite $destination with $source" git merge -s ours $source
            </code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>or</p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              # Hacky method to overwrite one branch with another in one commit git clean -dfx git
              checkout $destination git reset --hard $source git reset --soft ORIG_HEAD git add -fA
              . git rev-parse $source > .git/MERGE_HEAD git commit -m "Rewrite $destination with
              $source"
            </code>
          </pre>
          <div className="befaftpre" />
        </div>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  old: {
    title: "I am a bad person and must rewrite published histor",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          Hopefully you read the previous reference and fully understand why this is bad and what
          you have to tell everyone else to do in order to recover from this condition. Assuming
          this, you simply need to go to the parts of this document which assume that you have{" "}
          <em>not</em> yet pushed and do them as normal. Then you need to do a "force push"{" "}
          <code className="inline">git push -f</code> to thrust your updated history upon everyone
          else. As you read in the reference, this may be denied by default by your upstream
          repository (see <code className="inline">git config receive.denyNonFastForwards</code>,
          but can be disabled (temporarily I suggest) if you have access to the server. You then
          will need to send mail to everyone who <em>might</em> have pulled the history telling them
          that history was rewritten and they need to{" "}
          <code className="inline">git pull --rebase</code> and do a bit of history rewriting of
          their own if they branched or tagged from the now outdated history.
        </p>

        <p>
          Proceed with{" "}
          <a href="#unpushed" onclick="return(crumbs.both('#unpushed',this.innerHTML));">
            fixing the old commit
          </a>
          .
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  discardUnpushed: {
    title: "Discarding all local commits on this branch",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          In order to discard all local commits on this branch, to make the local branch identical
          to the "upstream" of this branch, simply run
          <code className="inline">
            git reset --hard @{"{"}u{"}"}
          </code>
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  replaceUnpushed: {
    title: "Replacing all branch history/contents",
    signPost:
      "If instead of discarding all local commits, you can make your branch identical to some other branch, tag, ref, or SHA that exists on your system.",
    solution: (
      <Fragment>
        <p>
          The first thing you need to do is identify the SHA or ref of the good state of your
          branch. You can do this by looking at the output of{" "}
          <code className="inline">git branch -a; git tag</code>,{" "}
          <code className="inline">git log --all</code> or, my preference, you can look graphically
          at <code className="inline">gitk --all --date-order</code>
        </p>

        <p>
          Once you have found the correct state of your branch, you can get to that state by
          running:
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git reset --hard REF</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>Obviously replace "REF" with the reference or SHA you want to get back to.</p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  fixUnpushed: {
    title: "Is the commit you want to fix the most recent?",
    signPost:
      "While the techniques mentioned to deal with deeper commits will work on the most recent, there are some convenient shortcuts you can take with the most recent commit.",
    solution: false,
    trails: [
      {
        sign: "Yes, I want to change the most recent commit",
        direction: "changeLast"
      },
      {
        sign: "Yes, I want to discard the most recent commit(s)",
        direction: "removeLast"
      },
      {
        sign:
          "Yes, I want to undo the last git operation(s) affecting the HEAD/tip of my branch (most useful for rebase, reset, or --amend)",
        direction: "undoTip"
      },
      {
        sign: "No, I want to change an older commit",
        direction: "changeDeep"
      },
      {
        sign: "No, I want to restore a older version of/deleted file as a new commit",
        direction: "restoreFile"
      },
      {
        sign: "Either way, I want to move a commit from one branch to another",
        direction: "moveCommit"
      }
    ]
  },

  // Step 7
  changeLast: {
    title: "Do you want to remove or change the commit message/contents of the last commit?",
    signPost: "",
    solution: false,
    trails: [
      {
        sign: "I want to remove the last commit",
        direction: "removeLast"
      },
      {
        sign: "I want to update the author/message/contents of the last commit",
        direction: "updateLast"
      },
      {
        sign: "I want to reorder, split, merge, or significantly rework the last commit(s)",
        direction: "reworkLast"
      }
    ]
  },
  removeLast: {
    title: "Removing the last commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          To remove the last commit from git, you can simply run{" "}
          <code className="inline">git reset --hard HEAD^</code> If you are removing multiple
          commits from the top, you can run <code className="inline">git reset --hard HEAD~2</code>{" "}
          to remove the last two commits. You can increase the number to remove even more commits.
        </p>

        <p>
          If you want to "uncommit" the commits, but keep the changes around for{" "}
          <a href="#reworkLast">reworking</a>, remove the "--hard":{" "}
          <code className="inline">git reset HEAD^</code> which will evict the commits from the
          branch and from the index, but leave the working tree around.
        </p>

        <p>
          If you want to save the commits on a new branch name, then run{" "}
          <code className="inline">git branch newbranchname</code> <em>before</em> doing the{" "}
          <code className="inline">git reset</code>.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  undoTip: {
    title: "Undoing the last few git operations affecting HEAD/my branch's tip",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          Practically every git operation which affects the repository is recorded in the git
          reflog. You may then use the reflog to look at the state of the branches at previous times
          or even go back to the state of the local branch at the time.
        </p>

        <p>
          While this happens for every git command affecting HEAD, it is usually most interesting
          when attempting to recover from a bad rebase or reset or an --amend'ed commit. There are
          better ways (listed by the rest of this document) from recovering from the more mundane
          reflog updates.
        </p>

        <p>
          The first thing you need to do is identify the SHA of the good state of your branch. You
          can do this by looking at the output of <code className="inline">git log -g</code> or, my
          preference, you can look graphically at{" "}
          <code className="inline">gitk --all --date-order $(git log -g --pretty=%H)</code>
        </p>

        <p>
          Once you have found the correct state of your branch, you can get back to that state by
          running
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git reset --hard SHA</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>You could also link that old state to a new branch name using</p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git checkout -b newbranch SHA</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>Obviously replace "SHA" in both commands with the reference you want to get back to.</p>

        <p>
          Note that any other commits you have performed since you did that "bad" operation will
          then be lost. You could <code className="inline">git cherry-pick</code> or{" "}
          <code className="inline">git rebase -p --onto</code>
          those other commits over.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  changeDeep: {
    title: "Do you want to remove an entire commit?",
    signPost: "",
    solution: false,
    trails: [
      {
        sign: "Yes, I want to remove an entire commit",
        direction: "removeDeep"
      },
      {
        sign: "No, I want to change an older commit",
        direction: "modifyDeep"
      }
    ]
  },
  moveCommit: {
    title: "Moving a commit from one branch to another",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          So, you have a commit which is in the wrong place and you want to move it from one branch
          to another. In order to do this, you will need to know the SHA of the first and last
          commit (in a continuous series of commits) you want to move (those values are the same if
          you are moving only one commit), the name of the branch you are moving the commit from,
          and the name of the branch you are moving the commit to. In the example below, I will name
          these four values $first, $last, $source, and $destination (respectively). Additionally,
          you will need to use a <a href="http://en.wiktionary.org/wiki/nonce">nonce</a>
          branch as a placeholder. I will call the nonce branch "nonce" in the following example.
          However, you may use any branch name that is not currently in use. You can delete it
          immediately after you are done.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git branch nonce $last git rebase -p --onto $destination $first^ nonce</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Remember that when you substitute $first in the command above, leave the "^" alone, it is
          literal.
        </p>

        <p>
          Use <code className="inline">gitk --all --date-order</code> to check to make sure the move
          looks correct (pretending that nonce is the destination branch). Please check very
          carefully if you were trying to move a merge, it may have been recreated improperly. If
          you <em>don't</em> like the result, you may delete the nonce branch (
          <code className="inline">git branch -D nonce</code>) and try again.
        </p>

        <p>
          However, if everything looks good, we can move the actual destination branch pointer to
          where nonce is:
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git checkout $destination git reset --hard nonce git branch -d nonce</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          If you double-checked with <code className="inline">gitk --all --date-order</code>, you
          would see that the destination branch looks correct. However, the commits are still on the
          source branch as well. We can get rid of those now:
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git rebase -p --onto $first^ $last $source</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Using <code className="inline">gitk --all --date-order</code> one last time, you should
          now see that the commits on the source branch have gone away. You have successfully moved
          the commits. Please check very carefully if merges occurred after the commits which were
          deleted. They may have been recreated incorrectly. If so you can either{" "}
          <a href="#undoTip" onclick="return(crumbs.both('#undoTip',this.innerHTML));">
            undo the delete
          </a>{" "}
          or try to delete the bad merge and try to recreate it manually, or create a fake (--ours)
          merge from the same SHA so that git is aware that the merge occurred.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },

  // Step 8
  updateLast: {
    title: "Updating the last commit's contents or commit message",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          To update the last commit's contents, author, or commit message for a commit which you
          have not pushed or otherwise published, first you need to get the index into the correct
          state you wish the commit to reflect. If you are changing the commit message only, you
          need do nothing. If you are changing the file contents, typically you would modify the
          working directory and use <code className="inline">git add</code> as normal.
        </p>

        <p>
          Note if you wish to restore a file to a known good state, you can use:{" "}
          <code className="inline">git checkout GOODSHA -- path/to/filename</code>.
        </p>

        <p>
          Once the index is in the correct state, then you can run{" "}
          <code className="inline">git commit --amend</code> to update the last commit. Yes, you can
          use "-a" if you want to avoid the <code className="inline">git add</code> suggested in the
          previous paragraph. You can also use --author to change the author information.
        </p>

        <p>
          If you want to do something more sophisticated that what "--amend" allows, please
          investigate <a href="#reworkLast">reworking</a> the last commit.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  reworkLast: {
    title: "Reworking the last commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          <b>WARNING:</b> These techniques should <em>only</em> be used for non-merge commits. If
          you have a merge commit, you are better off deleting the merge and recreating it.
        </p>

        <p>
          If you want to perform significant work on the last commit, you can simply{" "}
          <code className="inline">git reset HEAD^</code>. This will undo the commit (peel it off)
          and restore the index to the state it was in before that commit, leaving the working
          directory with the changes uncommitted, and you can fix whatever you need to fix and try
          again.
        </p>

        <p>
          You can do this with multiple (non-merge) commits in a row (using "HEAD^^" or similar
          techniques), but then of course you lose the separation between the commits and are left
          with an undifferentiated working directory. If you are trying to squash all of the commits
          together, or rework which bits are in which commits, this may be what you want.
        </p>

        <p>
          If you want to reorder commits, split them, merge them together, or otherwise perfect the
          commits, you should explore{" "}
          <a href="http://sethrobertson.github.com/GitPostProduction">
            Post Production Editing Using Git
          </a>
          .
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  removeDeep: {
    title: "Removing an entire commit",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          I call this operation "cherry-pit" since it is the inverse of a "cherry-pick". You must
          first identify the SHA of the commit you wish to remove. You can do this using{" "}
          <code className="inline">gitk --date-order</code> or using{" "}
          <code className="inline">git log --graph --decorate --oneline</code> You are looking for
          the 40 character SHA-1 hash ID (or the 7 character abbreviation). Yes, if you know the "^"
          or "~" shortcuts you may use those.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git rebase -p --onto SHA^ SHA</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Obviously replace "SHA" with the reference you want to get rid of. The "^" in that command
          is literal.
        </p>

        <p>
          However, please be warned. If some of the commits between SHA and the tip of your branch
          are merge commits, it is possible that <code className="inline">git rebase -p</code> will
          be unable to properly recreate them. Please inspect the resulting merge topology{" "}
          <code className="inline">gitk --date-order HEAD ORIG_HEAD</code>
          and contents to ensure that git did want you wanted. If it did not, there is not really
          any automated recourse. You can reset back to the commit before the SHA you want to get
          rid of, and then cherry-pick the normal commits and manually re-merge the "bad" merges. Or
          you can just suffer with the inappropriate topology (perhaps creating fake merges{" "}
          <code className="inline">git merge --ours otherbranch</code> so that subsequent
          development work on those branches will be properly merged in with the correct
          merge-base).
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  modifyDeep: {
    title:
      "Do you want to remove/change/rename a particular file/directory from all commits during all of git's history",
    signPost: "",
    solution: false,
    trails: [
      {
        sign: "Yes please, I want to make a change involving all git commits",
        direction: "bulkRewrite"
      },
      {
        sign: "No, I only want to change a single commit",
        direction: "singleDeepChange"
      }
    ]
  },

  // Step 9
  bulkRewrite: {
    title: "Changing all commits during all of git's history",
    signPost:
      "You have not pushed but still somehow want to change all commits in all of git's history? Strange.",
    solution: false,
    trails: [
      {
        sign:
          "Not just removing data (eg. re-arranging directory structure for all commits), or just wanting to use standard tools",
        direction: "filterBranch"
      },
      {
        sign:
          "Want to only remove unwanted data (big files, private data, etc) and am willing to use a third party tool to do the job more quickly",
        direction: "bfg"
      }
    ]
  },
  singleDeepChange: {
    title: "Is a merge commit involved?",
    signPost:
      "If the commit you are trying to change is a merge commit, or if there is a merge commit between the commit you are trying to change and the tip of the branch you are on, then you need to do some special handling of the situation.",
    solution: false,
    trails: [
      {
        sign: "Yes, a merge commit is involved",
        direction: "singleDeepMerge"
      },
      {
        sign: "No, only simple commits",
        direction: "singleDeepSimple"
      }
    ]
  },

  // Step 10
  filterBranch: {
    title: "Arbitrarily changing all commits during all of git's history",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          <code className="inline">git filter-branch</code> is a powerful, complex command that
          allows you to perform arbitary scriptable operations on all commits in git repository
          history. This flexibility can make it quite slow on big repos, and makes using the command
          quite difficult, so I will simply point you at the{" "}
          <a href="http://jk.gs/git-filter-branch.html">manual page</a>
          and remind you that{" "}
          <a href="http://sethrobertson.github.com/GitBestPractices/">best practice</a> is to always
          use <code>"--tag-name-filter cat -- --all"</code> unless you are really sure you know what
          you are doing.
        </p>

        <p>
          BTW, this is the one command I referred to earlier which will update all tags and
          branches, at least if you use the best practice arguments.
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  bfg: {
    title:
      "Use The BFG to remove unwanted data, like big files or passwords, from Git repository history.",
    signPost:
      "Disclaimer, the author of this document has not qualified this tool (including for safety or usability for any purpose), but recognizes the need which this tool fills for large repositories.",
    solution: (
      <Fragment>
        <p>
          <a href="http://rtyley.github.io/bfg-repo-cleaner/">The BFG</a> is a simpler, faster
          alternative to <code className="inline">git filter-branch</code>, specifically designed
          for cleansing bad data out of your Git repository history - it operates over all branches
          and tags in your project to purge data you don't want retained <strong>anywhere</strong>.
          Some
          <a href="http://rtyley.github.io/bfg-repo-cleaner/#examples">examples</a>:
        </p>

        <p>Remove all blobs bigger than 1 megabyte (to make your repo take up less space):</p>
        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              $ bfg <strong>--strip-blobs-bigger-than 1M</strong> my-repo.git
            </code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Replace all passwords listed in a file with <code className="inline">***REMOVED***</code>
          wherever they occur in your repository :
        </p>
        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>
              $ bfg <strong>--replace-text passwords.txt</strong> my-repo.git
            </code>
          </pre>
          <div className="befaftpre" />
        </div>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  singleDeepMerge: {
    title: "Changing a single commit involving a merge",
    signPost: (
      <Fragment>
        <p>
          Oh dear. This is going to get a little complicated. It should all work out, though. You
          will need to use a <a href="http://en.wiktionary.org/wiki/nonce">nonce</a> branch as a
          placeholder. I will call the nonce branch "nonce" in the following example. However, you
          may use any branch name that is not currently in use. You can delete it immediately after
          you are done.
        </p>
      </Fragment>
    ),
    solution: (
      <Fragment>
        <ul>
          <li>
            <p>Identify the SHA of the commit you wish to modify.</p>

            <p>
              You can do this using <code className="inline">gitk --date-order</code>
              or using <code className="inline">git log --graph --decorate --oneline</code> You are
              looking for the 40 character SHA-1 hash ID (or the 7 character abbreviation). Yes, if
              you know the "&#94;" or "~" shortcuts you may use those.
            </p>
          </li>

          <li>
            <p>Remember the name of the branch you are currently on</p>

            <p>
              The line with a star on it in the <code className="inline">git branch</code> output is
              the branch you are currently on. I will use "$master" in this example, but substitute
              your branch name for "$master" in the following commands.
            </p>
          </li>

          <li>
            <p>Create and checkout a nonce branch pointing at that commit.</p>

            <div className="prediv">
              <div className="befaftpre" />
              <pre>
                <code>git checkout -b nonce SHA</code>
              </pre>
              <div className="befaftpre" />
            </div>

            <p>Obviously replace "SHA" with the reference you want to modify.</p>
          </li>

          <li>
            <p>Modify the commit</p>

            <p>
              You need to get the index into the correct state you wish the commit to reflect. If
              you are changing the commit message only, you need do nothing. If you are changing the
              file contents, typically you would modify the working directory and use{" "}
              <code className="inline">git add</code> as normal.
            </p>

            <p>
              Note if you wish to restore a file to a known good state, you can use{" "}
              <code className="inline">git checkout GOODSHA -- path/to/filename</code>.
            </p>

            <p>
              Once the index is in the correct state, then you can run{" "}
              <code className="inline">git commit --amend</code> to update the last commit. Yes, you
              can use "-a" if you want to avoid the <code className="inline">git add</code>{" "}
              suggested in the previous paragraph.
            </p>

            <p>
              If the commit you are updating is a merge commit, ensure that the log message reflects
              that.
            </p>
          </li>

          <li>
            <p>Put the remaining commits after the new one you just created</p>

            <p>Remembering to substitute the correct branch name for $master</p>

            <div className="prediv">
              <div className="befaftpre" />
              <pre>
                <code>git rebase -p --onto $(git rev-parse nonce) HEAD^ $master</code>
              </pre>
              <div className="befaftpre" />
            </div>
          </li>
          <li>
            <p>Validate that the topology is still good</p>

            <p>
              If some of the commits after the commit you changed are merge commits, please be
              warned. It is possible that <code className="inline">git rebase -p</code> will be
              unable to properly recreate them. Please inspect the resulting merge topology{" "}
              <code className="inline">gitk --date-order HEAD ORIG_HEAD</code>
              to ensure that git did want you wanted. If it did not, there is not really any
              automated recourse. You can reset back to the commit before the SHA you want to get
              rid of, and then cherry-pick the normal commits and manually re-merge the "bad"
              merges. Or you can just suffer with the inappropriate topology (perhaps creating fake
              merges <code className="inline">git merge --ours otherbranch</code> so that subsequent
              development work on those branches will be properly merged in with the correct
              merge-base).
            </p>
          </li>

          <li>
            <p>Delete the nonce branch</p>

            <p>
              You don't need it. It was just there to communicate an SHA between two steps in the
              above process. <code className="inline">git branch -d nonce</code>
            </p>
          </li>
        </ul>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  },
  singleDeepSimple: {
    title: "Changing a single commit involving only simple commits",
    signPost: "",
    solution: (
      <Fragment>
        <p>
          You must first identify the SHA of the commit you wish to remove. You can do this using{" "}
          <code className="inline">gitk --date-order</code> or using{" "}
          <code className="inline">git log --graph --decorate --oneline</code>
          You are looking for the 40 character SHA-1 hash ID (or the 7 character abbreviation). Yes,
          if you know the "&#94;" or "~" shortcuts you may use those.
        </p>

        <div className="prediv">
          <div className="befaftpre" />
          <pre>
            <code>git rebase -i SHA^</code>
          </pre>
          <div className="befaftpre" />
        </div>

        <p>
          Obviously replace "SHA" with the reference you want to get rid of. The "&#94;" in that
          command is literal.
        </p>

        <p>
          You will be dumped in an editor with a bunch of lines starting with pick. The oldest
          commit, the one you are probably interested in changing, is first. You will want to change
          the "pick" to "reword" or "edit", or perhaps even "squash" depending on what your goal is.
          Please read the <a href="http://jk.gs/git-rebase.html">manual page</a>
          for more information. A document on{" "}
          <a href="http://sethrobertson.github.com/GitPostProduction/">
            Post-Production Editing using Git
          </a>{" "}
          goes through much of the major uses of git rebase is some detail. The use case is a little
          different, but the fundamental techniques are not.
        </p>

        <p>
          When using "edit", to change contents or author, when you are dumped into the shell to
          make your change, well make your change, <code className="inline">git add</code> as
          normal, and then run <code className="inline">git commit --amend</code> (including
          changing the author information with --author). When you are satisfied, you should run{" "}
          <code className="inline">git rebase --continue</code>
        </p>
      </Fragment>
    ),
    trails: [
      {
        sign: "Start over",
        direction: "start"
      }
    ]
  }
}

export default trails
