/*** IMPORTS ***/
// Module imports
import React, { Component } from "react"

// Styles
import "./style.sass"

// Components
import Header from "./components/Header"
import Breadcrumbs from "./components/Breadcrumbs"
import Tile from "./components/Tile"
import Footer from "./components/Footer"

// Data
import trails from "./data/trails"
/*** [end of imports] ***/

// credit: http://sethrobertson.github.io/GitFixUm/fixup.html

export default class App extends Component {
  state = {
    crumbs: [],
    tile: "start"
  }

  goTo = (lastTile, newTile) => {
    let { crumbs } = this.state

    if (newTile === "start") {
      this.setState({
        crumbs: [],
        tile: newTile
      })
    } else {
      crumbs.push(lastTile)
      this.setState({
        crumbs,
        tile: newTile
      })
    }
  }

  render() {
    const { crumbs, tile } = this.state

    return (
      <div className="app">
        <Header goTo={this.goTo} />

        <main className="app__main">
          <Breadcrumbs crumbs={crumbs} />
          <Tile tile={trails[tile]} goTo={this.goTo} />
        </main>

        <Footer />
      </div>
    )
  }
}
